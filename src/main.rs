use std::fs::File;
use std::io;
use iced::{
    Align, Settings, Element, Sandbox, button, Button, Column, Text, TextInput, text_input};
mod markDown;

#[derive(Default)]
struct Lander{
    value: i32,
    create_recipe:      button::State,
    update_recipe:      button::State,
    recipe_title:       text_input::State,
    title:              String,
    recipe_ingredients: text_input::State,
    ingredients:        String,
    recipe_steps:       text_input::State,
    steps:              String,
    recipe_macros:      text_input::State,
    macros:             String
}

#[derive(Debug, Clone, Copy)]
pub enum Message {
    CreateRecipe,
    UpdateRecipe,
    TitleReceived,
    SendTitle,
}

impl Sandbox for Lander{
    type Message = Message;
    fn new() -> Self{
        Self::default()
    }

    fn title(&self) -> String{
        String::from("Counter - Iced")
    }
    fn view(&mut self) -> Element<Message>{
        let input =  TextInput::new(
                                input,
                                "What is the title of your recipe?",
                                &self.title,
                                Message::TitleReceived,
                            );
        let buttons = Column::new()
            .padding(20)
            .align_items(Align::End)
            .push(
                Button::new(&mut self.create_recipe, Text::new("Create Recipe"))
                    .on_press(Message::CreateRecipe),
            )
            .push(
                Button::new(&mut self.update_recipe, Text::new("Update Recipe"))
                    .on_press(Message::UpdateRecipe),
            )
            .push(

            ).into();
        
        return buttons;
    }
    fn update(&mut self, message: Message){
        match message {
            Message::CreateRecipe=> {
                self.value += 1;
            }
            Message::UpdateRecipe=> {
                self.value -= 1;
            }
        }
    }
}
fn main() {
    Lander::run(Settings::default());
    println!("What category are you adding the recipe to?");
    let mut category =  String::new();

    io::stdin().read_line(&mut category)
        .expect("Failed to read line");

    category.pop();
    println!("What is the title of the recipe you would like to add?");
    let mut title =  String::new();

    io::stdin().read_line(&mut title)
        .expect("Failed to read line");
    title.pop();
    let mut start = markDown::Recipe::new(&title, &category);
//    start.add_ingredient("Beef     4oz");
//    let mut ingred = String::new();
//    println!("What is the next ingredient you would like to add? If you are done enter n");
//    io::stdin().read_line(&mut ingred)
//        .expect("Failed to read line");
//    ingred.pop();
//   while &ingred != "n"{
//       start.add_ingredient(&ingred);
//       println!("What is the next ingredient you would like to add? If you are done enter n");
//       ingred = String::new();
//       io::stdin().read_line(&mut ingred)
//           .expect("Failed to read line");
//        ingred.pop();
//        }
}
