use std::fs::File;
use std::fs;
use std::io::Write;
use std::path::PathBuf;
use std::path::Path;

pub struct Recipe{
    step_count: u32,
    file: File,
    path: PathBuf,
}
impl Recipe{
    //Function for 
    pub fn new(title: &str, category: &str) -> Recipe{
        let mut pathes = String::from("/home/mekhi/rpee/recipies/");
        pathes.push_str(category);
        fs::create_dir_all(Path::new(&pathes));
        pathes.push_str("/");
        pathes.push_str(title);
        pathes.push_str(".md");
        let smash = Path::new(&pathes);
        let mut spike = Recipe{
            path: PathBuf::from(&pathes),
            file: File::create(&smash).expect("uh oh"),
            step_count: 0,
        }; 
        let mut header:String = String::from("# ");
        header.push_str(title);
        header.push_str("\n");
        let IngredTitle = String::from("Ingredients:\n");
        let NuttyTitle = String::from("Nutrition:\n");
        let StepTitle = String::from("Steps:\n");
        spike.file.write_all(header.as_bytes());
        spike.file.write_all(IngredTitle.as_bytes());
        spike.file.write_all(StepTitle.as_bytes());
        spike.file.write_all(NuttyTitle.as_bytes());
        return spike;
    }

    pub fn add_ingredient(&mut self, ingredient: &str){
        let mut f = String::from("\n   ");
        f.push_str(ingredient);
        self.file.write_all(f.as_bytes());
    }
    pub fn add_instructions(&mut self, step: &str){
       let mut this_step = String::from("\n  This is a step"); 
       this_step.push_str(step);
       self.file.write_all(this_step.as_bytes());
    }
    pub fn add_nutrition(&mut self, nutrient: &str){
       let mut nutrients= String::from("\n  "); 
       nutrients.push_str(nutrient);
       self.file.write_all(nutrients.as_bytes());
    }
}
